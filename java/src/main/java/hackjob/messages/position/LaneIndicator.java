package hackjob.messages.position;

public class LaneIndicator {

    private int startLaneIndex;
    private int endLaneIndex;

    public boolean changingLanes() {
        return startLaneIndex != endLaneIndex;
    }
}
