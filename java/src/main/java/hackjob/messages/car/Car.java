package hackjob.messages.car;

public class Car {

    private CarId id;
    private Dimensions dimensions;

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", dimensions=" + dimensions +
                '}';
    }
}