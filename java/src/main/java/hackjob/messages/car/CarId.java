package hackjob.messages.car;

public class CarId {

    private String name;
    private String color;

    @Override
    public String toString() {
        return "Id{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
