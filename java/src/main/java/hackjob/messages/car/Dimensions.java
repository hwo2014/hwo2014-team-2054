package hackjob.messages.car;

public class Dimensions {

    private double length;
    private double width;
    private double guideFlagPosition;

    @Override
    public String toString() {
        return "Dimensions{" +
                "length=" + length +
                ", width=" + width +
                ", guideFlagPosition=" + guideFlagPosition +
                '}';
    }
}