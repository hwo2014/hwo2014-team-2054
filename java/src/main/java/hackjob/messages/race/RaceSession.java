package hackjob.messages.race;

public class RaceSession {

    private int laps;
    private long maxLapTimeMs;
    private boolean quickRace;

    @Override
    public String toString() {
        return "RaceSession{" +
                "laps=" + laps +
                ", maxLapTimeMs=" + maxLapTimeMs +
                ", quickRace=" + quickRace +
                '}';
    }
}