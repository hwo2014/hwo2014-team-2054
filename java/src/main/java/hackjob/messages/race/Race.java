package hackjob.messages.race;

import hackjob.messages.car.Car;
import hackjob.messages.race.track.Track;

import java.util.List;

public class Race {

    private Track track;
    private List<Car> cars;
    private RaceSession raceSession;

    @Override
    public String toString() {
        return "Race{" +
                "track=" + track +
                ", cars=" + cars +
                ", raceSession=" + raceSession +
                '}';
    }
}
