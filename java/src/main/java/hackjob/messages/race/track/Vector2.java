package hackjob.messages.race.track;

public class Vector2 {

    private double x;
    private double y;

    @Override
    public String toString() {
        return "Vector2{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
