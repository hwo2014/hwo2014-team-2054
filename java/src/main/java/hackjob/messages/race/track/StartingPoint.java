package hackjob.messages.race.track;

public class StartingPoint {

    private Vector2 position;
    private double angle;

    @Override
    public String toString() {
        return "StartingPoint{" +
                "position=" + position +
                ", angle=" + angle +
                '}';
    }
}
