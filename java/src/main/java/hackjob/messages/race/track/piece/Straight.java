package hackjob.messages.race.track.piece;

public class Straight extends Piece {

    private double length;

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Straight{" +
                "length=" + length +
                ", switch=" + isSwitch +
                '}';
    }
}
