package hackjob.messages.race.track.piece;

import com.google.gson.annotations.SerializedName;

public abstract class Piece {

    @SerializedName("switch")
    protected boolean isSwitch;

    public boolean isSwitch() {
        return isSwitch;
    }

    public abstract double getLength();
}
