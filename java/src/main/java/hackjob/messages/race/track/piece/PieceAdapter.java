package hackjob.messages.race.track.piece;

import com.google.gson.*;

import java.lang.reflect.Type;

public class PieceAdapter implements JsonSerializer<Piece>, JsonDeserializer<Piece> {

    @Override
    public Piece deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        return context.deserialize(jsonObject, jsonObject.get("length") != null ? Straight.class : Curve.class);
    }

    @Override
    public JsonElement serialize(Piece src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src);
    }
}
