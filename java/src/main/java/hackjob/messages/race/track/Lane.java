package hackjob.messages.race.track;

public class Lane {

    private double distanceFromCenter;
    private int index;

    @Override
    public String toString() {
        return "Lane{" +
                "distanceFromCenter=" + distanceFromCenter +
                ", index=" + index +
                '}';
    }
}
