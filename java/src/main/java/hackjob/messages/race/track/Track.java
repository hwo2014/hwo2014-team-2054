package hackjob.messages.race.track;

import hackjob.messages.race.track.piece.Piece;

import java.util.List;

public class Track {

    private String id;
    private String name;
    private List<Piece> pieces;
    private List<Lane> lanes;
    private StartingPoint startingPoint;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public StartingPoint getStartingPoint() {
        return startingPoint;
    }

    @Override
    public String toString() {
        return "Track{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", pieces=" + pieces +
                ", lanes=" + lanes +
                ", startingPoint=" + startingPoint +
                '}';
    }
}