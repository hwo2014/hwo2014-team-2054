package hackjob.messages.race.track.piece;

public class Curve extends Piece {

    private double radius;
    private double angle;

    @Override
    public double getLength() {
        return angle * radius / 180.0;
    }

    @Override
    public String toString() {
        return "Curve{" +
                "radius=" + radius +
                ", angle=" + angle +
                ", switch=" + isSwitch +
                '}';
    }
}
