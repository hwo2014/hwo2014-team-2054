package hackjob.messages.result;

import com.google.gson.*;

import java.lang.reflect.Type;

public class ResultAdapter implements JsonSerializer<Result>, JsonDeserializer<Result> {

    @Override
    public Result deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        return context.deserialize(jsonObject, jsonObject.get("laps") != null ? RaceResult.class : BestLapResult.class);
    }

    @Override
    public JsonElement serialize(Result src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src);
    }
}