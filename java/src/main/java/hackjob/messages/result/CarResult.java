package hackjob.messages.result;

import hackjob.messages.car.CarId;

public class CarResult {

    private CarId car;
    private Result result;

    public CarId getCar() {
        return car;
    }

    public Result getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "CarResult{" +
                "car=" + car +
                ", result=" + result +
                '}';
    }
}
