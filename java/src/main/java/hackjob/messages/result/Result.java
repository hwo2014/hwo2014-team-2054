package hackjob.messages.result;

public abstract class Result {

    private long ticks;
    private long millis;

    public long getTicks() {
        return ticks;
    }

    public long getMillis() {
        return millis;
    }
}
