package hackjob.messages;

public enum MsgType {

    JOIN("join"),
    YOUR_CAR("yourCar"),
    GAME_INIT("gameInit"),
    GAME_START("gameStart"),
    POSITIONS("carPositions"),
    THROTTLE("throttle"),
    GAME_END("gameEnd"),
    TOURNAMENT_END("tournamentEnd"),
    CRASH("crash"),
    SPAWN("spawn"),
    LAP_FINISHED("lapFinished"),
    DISQUALIFIED("dnf"),
    FINISH("finish"),
    SWITCH_LANE("switchLane"),
    CREATE_RACE("createRace"),
    JOIN_RACE("joinRace"),
    PING("ping");

    private final String name;

    private MsgType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static MsgType getByName(String name) {
        for (MsgType msgType : MsgType.values()) {
            if (msgType.name.equalsIgnoreCase(name)) {
                return msgType;
            }
        }
        return null;
    }
}
