package hackjob.messages.send;

public class Join extends SendMsg {

    public final String name;
    public final String key;

    public Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    public String msgType() {
        return "join";
    }
}
