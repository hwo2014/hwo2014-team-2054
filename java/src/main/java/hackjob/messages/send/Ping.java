package hackjob.messages.send;

public class Ping extends SendMsg {

    @Override
    public String msgType() {
        return "ping";
    }
}
