package hackjob;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import hackjob.messages.MsgType;
import hackjob.messages.car.CarId;
import hackjob.messages.position.CarPosition;
import hackjob.messages.race.Race;
import hackjob.messages.race.track.piece.Piece;
import hackjob.messages.race.track.piece.PieceAdapter;
import hackjob.messages.result.CarResult;
import hackjob.messages.result.Result;
import hackjob.messages.result.ResultAdapter;
import hackjob.messages.send.Join;
import hackjob.messages.send.Ping;
import hackjob.messages.send.SendMsg;
import hackjob.messages.send.Throttle;

import java.io.*;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.List;

public class App {

    private final JsonParser parser = new JsonParser();
    private final Gson gson;
    private PrintWriter writer;

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        new App(reader, writer, new Join(botName, botKey));
    }

    public App(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Piece.class, new PieceAdapter());
        builder.registerTypeAdapter(Result.class, new ResultAdapter());
        gson = builder.create();

        this.writer = writer;
        String line;

        send(join);

        while((line = reader.readLine()) != null) {
            JsonElement element = parser.parse(line);
            if (element.isJsonObject()) {
                JsonObject object = (JsonObject) element;
                JsonElement typeElement = object.get("msgType");
                JsonElement dataElement = object.get("data");
                if (typeElement != null) {
                    try {
                        MsgType msgType = MsgType.getByName(typeElement.getAsString());
                        switch (msgType) {
                            case YOUR_CAR:
                                System.out.println("Your car");
                                CarId carId = gson.fromJson(dataElement.getAsJsonObject(), CarId.class);
                                System.out.println(carId);
                            case POSITIONS:
                                System.out.println("Received positions");
                                Type collectionType = new TypeToken<List<CarPosition>>(){}.getType();
                                List<CarPosition> carPositions = gson.fromJson(dataElement.getAsJsonArray(), collectionType);
                                send(new Throttle(0.65));
                                break;
                            case JOIN:
                                System.out.println("Joined");
                                break;
                            case GAME_INIT:
                                System.out.println("Race init");
                                Race race = gson.fromJson(dataElement.getAsJsonObject().get("race"), Race.class);
                                System.out.println(race);
                                break;
                            case GAME_END:
                                System.out.println("Race end");

                                Type carResultsType = new TypeToken<List<CarResult>>(){}.getType();
                                List<CarResult> carResults = gson.fromJson(dataElement.getAsJsonObject().get("results").getAsJsonArray(), carResultsType);
                                List<CarResult> bestLapResults = gson.fromJson(dataElement.getAsJsonObject().get("bestLaps").getAsJsonArray(), carResultsType);

                                break;
                            case GAME_START:
                                System.out.println("Race start");
                                break;
                            default:
                                System.out.println("Unknown message received");
                                send(new Ping());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        send(new Ping());
                    }
                }
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

